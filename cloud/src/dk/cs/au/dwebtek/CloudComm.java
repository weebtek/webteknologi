package dk.cs.au.dwebtek;

import com.sun.corba.se.spi.orb.Operation;
import dk.cs.au.dwebtek.requests.AdjustItemStock;
import dk.cs.au.dwebtek.requests.ModifyItem;
import dk.cs.au.dwebtek.requests.PostRequest;
import dk.cs.au.dwebtek.requests.Request;
import org.jdom2.Document;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.input.sax.XMLReaderJDOMFactory;
import org.jdom2.input.sax.XMLReaderXSDFactory;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import org.jdom2.transform.JDOMSource;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.print.Doc;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by mortenkrogh-jespersen on 07/02/2017.
 */
public class CloudComm {

    public static <E> OperationResult<E> performRequest(Request req) {


        try {
            boolean isPost = req instanceof PostRequest;

            URL url = new URL("http://webtek.cs.au.dk/cloud" + req.getPath());

            HttpURLConnection conn = (HttpURLConnection)url.openConnection();

            if (isPost) {
                PostRequest<E> postReq = (PostRequest<E>)req;
                // Set up a post-request - remember to validate
                conn.setRequestMethod("POST");
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.connect();

                OperationResult post = postReq.getPostBody();

                // returnerer error message hvis getBody failer
                if (!post.isSuccess()){
                return post;
                }

                Document doc = (Document)post.getResult();

                OperationResult validation = new CloudComm().validate(doc);

                if(!validation.isSuccess()){
                    System.out.println(validation.getResult());
                    return validation;
                }

                if (validation.isSuccess()) {
                    // Print the xml in question
                    XMLOutputter xmlOutputter = new XMLOutputter(Format.getPrettyFormat());
                    System.out.println(xmlOutputter.outputString(doc));
                    xmlOutputter.output(doc,conn.getOutputStream());
                }

            } else {
                conn.setRequestMethod("GET");
                conn.setDoInput(true);
                conn.connect();
            }

            System.out.println(conn.getResponseCode() + req.getPath());

            if (conn.getResponseCode() == 200) {    /* 200 OK*/
                // Modify response
                InputStreamReader inputStreamReader = new InputStreamReader(conn.getInputStream());
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                StringBuilder stringBuilder = new StringBuilder();
                String read;
                read = bufferedReader.readLine();

                while (read != null) {
                    stringBuilder.append(read);
                    read = bufferedReader.readLine();
                }

                // No response requests goes here
                if (req instanceof ModifyItem || req instanceof AdjustItemStock) {
                    return OperationResult.Success();
                }

                // Convert to doc
                SAXBuilder sb = new SAXBuilder();
                Document doc = sb.build(new StringReader(stringBuilder.toString()));

                // Pretty printing
               // XMLOutputter xmlOutputter = new XMLOutputter(Format.getPrettyFormat());
               // System.out.println(xmlOutputter.outputString(doc));

                //System.out.println(stringBuilder.toString());
                if (new CloudComm().validate(doc).isSuccess()) {
                    return OperationResult.Success((E) req.parseResponse(stringBuilder.toString()));
                }
            }else {
                return OperationResult.Fail("Not correct responsecode: " + conn.getResponseCode());
            }

        } catch (IOException | JDOMException e) {
            e.printStackTrace();
        }
        return null;
    }


    private OperationResult<Object> validate(Document doc) {

        URL url = getClass().getClassLoader().getResource("cloud.xsd");
        XMLReaderJDOMFactory factory;

        try {
            factory = new XMLReaderXSDFactory(url);
        } catch (JDOMException e) {
            return OperationResult.Fail("Could not find schema");
        }

        String xml = new XMLOutputter().outputString(doc);
        SAXBuilder builder = new SAXBuilder(factory);
        try {
            builder.build(new StringReader(xml));
        } catch (JDOMException e) {
            return OperationResult.Fail("Xml is not valid: " + e.getMessage());
        } catch (IOException e) {
            return OperationResult.Fail("YIKES: " + e.getMessage());
        }
        return OperationResult.Success(true);
    }

}