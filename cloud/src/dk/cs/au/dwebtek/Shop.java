package dk.cs.au.dwebtek;

/**
 * Created by tiami on 08/03/2017.
 */
public class Shop {

    private int shopID;
    private String shopURL;
    private String shopName;

    public Shop(int shopID, String shopURL, String shopName)
    {
        this.shopID = shopID;
        this.shopURL = shopURL;
        this.shopName = shopName;
    }

    public int getShopID() {
        return shopID;
    }

    public void setShopID(int shopID) {
        this.shopID = shopID;
    }

    public String getShopURL() {
        return shopURL;
    }

    public void setShopURL(String shopURL) {
        this.shopURL = shopURL;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }
}
