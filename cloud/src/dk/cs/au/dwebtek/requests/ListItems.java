package dk.cs.au.dwebtek.requests;

import dk.cs.au.dwebtek.Item;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.Namespace;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Anne on 17/02/2017.
 */
public class ListItems implements Request {

    private int shopID;
    private static final Namespace NS = Namespace.getNamespace("http://www.cs.au.dk/dWebTek/2014");

    public ListItems(int shopID) {
        this.shopID = shopID;
    }

    @Override
    public String getPath() {
        return "/listItems?shopID=" + shopID;
    }

    @Override
    public List<Item> parseResponse(String message) throws JDOMException, IOException {

        SAXBuilder sb = new SAXBuilder();
        Document doc = sb.build(new StringReader(message));
        Element root = doc.getRootElement();

        List<Item> liste = new ArrayList<>();

        for (Element item : root.getChildren()) {
            Item i = new Item(0,"",0);
            i.setId(Integer.parseInt(item.getChildText("itemID", NS)));
            i.setName(item.getChildText("itemName", NS));
            i.setPrice(Integer.parseInt(item.getChildText("itemPrice", NS)));
            i.setStock(Integer.parseInt(item.getChildText("itemStock", NS)));
            i.setUrl(item.getChildText("itemURL", NS));

            for (Element child : item.getChild("itemDescription", NS).getChild("document", NS).getChildren()){
                transform(child);
            }

            XMLOutputter xmlOutputter = new XMLOutputter(Format.getPrettyFormat());
            i.setItemDescription(xmlOutputter.outputElementContentString(item.getChild("itemDescription", NS).getChild("document", NS)));

            liste.add(i);
        }
        return liste;
    }

    //Rekursiv metode for element i itemDescription
    private void transform(Element element){

        Element child = element;

        child.setNamespace(Namespace.getNamespace("http://www.w3.org/1999/xhtml"));

        if (child.getName().equals("list")){
            child.setName("ul");
        }

        else if (child.getName().equals("bold")){
            child.setName("strong");
        }

        else if (child.getName().equals("item")){
            child.setName("li");
        }

        else if (child.getName().equals("italics")){
            child.setName("em");
        }

        for (Element e : child.getChildren()) {
            transform(e);
        }
    }
}