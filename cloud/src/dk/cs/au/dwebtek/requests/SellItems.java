package dk.cs.au.dwebtek.requests;

import dk.cs.au.dwebtek.OperationResult;
import org.jdom2.*;
import org.jdom2.filter.ElementFilter;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.XMLOutputter;

import java.io.IOException;
import java.io.StringReader;

/**
 * Created by tiami on 08/03/2017.
 */
public class SellItems implements PostRequest  {

    private int itemID;
    private int customerID;
    private int amount;
    private static final String SHOP_KEY = "00FF0887059C03DCC03AB517";
    private static final Namespace NS = Namespace.getNamespace("http://www.cs.au.dk/dWebTek/2014");

    public SellItems(int itemID, int customerID, int amount) {
        this.itemID = itemID;
        this.customerID = customerID;
        this.amount = amount;
    }

    @Override
    public String getPath() {
        return "/sellItems";
    }

    @Override
    public Document parseResponse(String message) {

        SAXBuilder sb = new SAXBuilder();
        try {
            Document doc = sb.build(new StringReader(message));

            return doc;
        } catch (JDOMException | IOException e) {
            e.printStackTrace();
        }
        System.out.println("PARSING FAILED");
        return null;
    }

    @Override
    public OperationResult getPostBody() {

        Element shopKey = new Element("shopKey", NS);
        shopKey.addContent(SHOP_KEY);

        Element itemid = new Element("itemID", NS);
        itemid.addContent(String.valueOf(itemID));

        Element customerid = new Element("customerID", NS);
        customerid.addContent(String.valueOf(customerID));

        Element saleAmount = new Element("saleAmount", NS);
        saleAmount.addContent(String.valueOf(amount));

        Element result = new Element("sellItems", NS);
        result.addContent(shopKey);
        result.addContent(itemid);
        result.addContent(customerid);
        result.addContent(saleAmount);

        Document doc = new Document();
        doc.setRootElement(result);

        return OperationResult.Success(doc);

    }
}