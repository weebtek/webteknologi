package dk.cs.au.dwebtek.requests;

import dk.cs.au.dwebtek.OperationResult;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.Namespace;

import java.io.IOException;

/**
 * Created by Anne on 24/02/2017.
 */
public class AdjustItemStock implements PostRequest {

    private static final String SHOP_KEY = "00FF0887059C03DCC03AB517";
    private static final Namespace NS = Namespace.getNamespace("http://www.cs.au.dk/dWebTek/2014");
    private int itemID, adjustmentNumber;

    public AdjustItemStock(int itemID, int adjustmentNumber) {
        this.itemID = itemID;
        this.adjustmentNumber = adjustmentNumber;
    }

    @Override
    public String getPath() {
        return "/adjustItemStock";
    }

    @Override
    public Object parseResponse(String message) throws JDOMException, IOException {
        return null;
    }

    @Override
    public OperationResult getPostBody() {

        Element shopKey = new Element("shopKey", NS);
        shopKey.addContent(SHOP_KEY);

        Element id = new Element("itemID", NS);
        id.addContent(String.valueOf(itemID));

        Element adjustment = new Element("adjustment", NS);
        adjustment.addContent(String.valueOf(adjustmentNumber));

        Element result = new Element("adjustItemStock", NS);
        result.addContent(shopKey);
        result.addContent(id);
        result.addContent(adjustment);

        Document doc = new Document();
        doc.setRootElement(result);

        return OperationResult.Success(doc);
    }
}
