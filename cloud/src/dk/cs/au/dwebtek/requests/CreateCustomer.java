package dk.cs.au.dwebtek.requests;

import dk.cs.au.dwebtek.OperationResult;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.Namespace;
import org.jdom2.input.SAXBuilder;

import java.io.IOException;
import java.io.StringReader;

/**
 * Created by tiami on 06/03/2017.
 */
public class CreateCustomer implements PostRequest {

    private String navn;
    private String password;
    private static final String SHOP_KEY = "00FF0887059C03DCC03AB517";
    private static final Namespace NS = Namespace.getNamespace("http://www.cs.au.dk/dWebTek/2014");

    public CreateCustomer(String navn, String password) {
        this.navn = navn;
        this.password = password;
    }

    @Override
    public String getPath() {
        return "/createCustomer";
    }

    @Override
    public Document parseResponse(String message) {

        //converting to document
        SAXBuilder sb = new SAXBuilder();
        try {
            Document doc = sb.build(new StringReader(message));

            return doc;
        } catch (JDOMException | IOException e) {
            e.printStackTrace();
        }
        System.out.println("PARSING FAILED");
        return null;
    }

    @Override
    public OperationResult getPostBody() {

        Element shopKey = new Element("shopKey", NS);
        shopKey.addContent(SHOP_KEY);

        Element customerName = new Element("customerName", NS);
        customerName.addContent(String.valueOf(navn));

        Element customerPass = new Element("customerPass", NS);
        customerPass.addContent(String.valueOf(password));

        Element result = new Element("createCustomer", NS);
        result.addContent(shopKey);
        result.addContent(customerName);
        result.addContent(customerPass);

        Document doc = new Document();
        doc.setRootElement(result);

        return OperationResult.Success(doc);
    }

}

