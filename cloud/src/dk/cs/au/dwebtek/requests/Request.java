package dk.cs.au.dwebtek.requests;

import org.jdom2.JDOMException;

import java.io.IOException;

/**
 * Created by mortenkrogh-jespersen on 07/02/2017.
 */
public interface Request<E> {

    String getPath();
    E parseResponse(String message) throws JDOMException, IOException;

}