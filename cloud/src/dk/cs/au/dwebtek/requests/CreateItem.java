package dk.cs.au.dwebtek.requests;

import dk.cs.au.dwebtek.OperationResult;
import org.jdom2.*;
import org.jdom2.filter.ElementFilter;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.XMLOutputter;

import java.io.IOException;
import java.io.StringReader;

/**
 * Created by mortenkrogh-jespersen on 07/02/2017.
 */
public class CreateItem implements PostRequest<Integer>  {

    private String name;
    private static final String SHOP_KEY = "00FF0887059C03DCC03AB517";
    private static final Namespace NS = Namespace.getNamespace("http://www.cs.au.dk/dWebTek/2014");

    public CreateItem(String name) {
        this.name = name;
    }

    @Override
    public String getPath() {
        return "/createItem";
    }

    @Override
    public Integer parseResponse(String message) {
        // Convert to doc
        SAXBuilder sb = new SAXBuilder();
        try {
            Document doc = sb.build(new StringReader(message));
            Element root = doc.getRootElement();
            Content content = root.getContent().get(0);
            //System.out.println("TEST:" + content.getValue());

            // return itemID, assuming index 0 is itemID
            return Integer.parseInt(content.getValue());
        } catch (JDOMException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public OperationResult getPostBody() {

        Element shopKey = new Element("shopKey", NS);
        shopKey.addContent(SHOP_KEY);

        Element itemName = new Element("itemName", NS);
        itemName.addContent(name);

        Element result = new Element("createItem", NS);
        result.addContent(shopKey);
        result.addContent(itemName);

        Document doc = new Document();
        doc.setRootElement(result);

        return OperationResult.Success(doc);

    }
}