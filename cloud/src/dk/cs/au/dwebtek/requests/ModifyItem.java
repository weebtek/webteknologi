package dk.cs.au.dwebtek.requests;

import dk.cs.au.dwebtek.OperationResult;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.Namespace;
import org.jdom2.input.SAXBuilder;

import java.io.IOException;
import java.io.StringReader;
import java.util.Iterator;

/**
 * Created by Anne on 17/02/2017.
 */
public class ModifyItem implements PostRequest{

    private static final String SHOP_KEY = "00FF0887059C03DCC03AB517";
    private static final Namespace NS = Namespace.getNamespace("http://www.cs.au.dk/dWebTek/2014");
    private int itemID;
    private String itemName;
    private int itemPrice;
    private String itemURL;
    private String itemDescription;

    public ModifyItem (int itemID, String itemName, int itemPrice, String itemURL, String itemDescription){

        this.itemID = itemID;
        this.itemName = itemName;
        this.itemPrice = itemPrice;
        this.itemURL = itemURL;
        this.itemDescription = itemDescription;
    }

    @Override
    public String getPath() {
        return "/modifyItem";
    }

    @Override
    public Object parseResponse(String message) {
        return message;
    }

    @Override
    public OperationResult getPostBody() {

        // itemDescription is a string, possible with XML content. We have to transform it.
        OperationResult<Element> itemDescRes = convertItemDescription(itemDescription);

        //if the parsing process is gone wrong
        if(itemDescRes.isSuccess()==false){
            return OperationResult.Fail(itemDescRes.getMessage());
        }

        setNamespace(itemDescRes.getResult());

        Element shopKey = new Element("shopKey", NS);
        shopKey.addContent(SHOP_KEY);

        Element id = new Element("itemID",NS);
        id.addContent(String.valueOf(itemID));

        Element name = new Element("itemName",NS);
        name.addContent(itemName);

        Element price = new Element("itemPrice",NS);
        price.addContent(String.valueOf(itemPrice));

        Element url = new Element("itemURL",NS);
        url.addContent(itemURL);

        Element description =  new Element("itemDescription",NS).addContent(itemDescRes.getResult());

        Element result = new Element("modifyItem",NS);
        result.addContent(shopKey);
        result.addContent(id);
        result.addContent(name);
        result.addContent(price);
        result.addContent(url);
        result.addContent(description);

        Document doc = new Document();
        doc.setRootElement(result);

        return OperationResult.Success(doc);
    }

    /**
     * Converts a string to an element.
     *
     * @param content the body of the document-element
     * @return A OperationResult where the result is an element if the xml is well-formed
     */
    private OperationResult<Element> convertItemDescription(String content) {
        String hi = new String("<document>" + content + "</document>");
        SAXBuilder b = new SAXBuilder();


        Document d = null;
        try {
            d = b.build(new StringReader(hi));
        } catch (JDOMException e) {
            return OperationResult.Fail("JDOM except: XML er forkert" + e.getMessage());
        } catch (IOException e) {
            return OperationResult.Fail("IO except: " + e.getMessage());
        }

        return OperationResult.Success(d.getRootElement().detach());

        // HINT: surround the content with "<document>" and "</document>" before parsing
    }

    /**
     * Sets the namespace on the element - what about the children??
     *
     * @param child the xml-element to have set the namespace
     */
    private void setNamespace(Element child) {
        child.setNamespace(NS);

        for (Element element : child.getChildren()) {
            setNamespace(element);
        }
//
//        Iterator i = child.getChildren().iterator();
//        while (i.hasNext()) {
//            child = (Element) (i.next());
//            child.setNamespace(NS);
//        }
    }

}