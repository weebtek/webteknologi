package dk.cs.au.dwebtek.requests;

import dk.cs.au.dwebtek.OperationResult;
import org.jdom2.*;
import org.jdom2.input.SAXBuilder;

import java.io.IOException;
import java.io.StringReader;

/**
 * Created by tiami on 06/03/2017.
 */
public class Login implements PostRequest {

    private String navn;
    private String password;
    private static final Namespace NS = Namespace.getNamespace("http://www.cs.au.dk/dWebTek/2014");

    public Login(String navn, String password) {
        this.navn = navn;
        this.password = password;
    }

    @Override
    public String getPath() {
        return "/login";
    }

    @Override
    public Document parseResponse(String message) {

        SAXBuilder sb = new SAXBuilder();
        try {
            Document doc = sb.build(new StringReader(message));

            return OperationResult.Success(doc).getResult();
        } catch (JDOMException | IOException e) {
            e.printStackTrace();
        }
        System.out.println("PARSING FAILED");
        return null;
    }

    @Override
    public OperationResult getPostBody() {

        Element costumerName = new Element("customerName", NS);
        costumerName.addContent(String.valueOf(navn));

        Element costumerPass = new Element("customerPass", NS);
        costumerPass.addContent(String.valueOf(password));

        Element result = new Element("login", NS);
        result.addContent(costumerName);
        result.addContent(costumerPass);

        Document doc = new Document();
        doc.setRootElement(result);

        return OperationResult.Success(doc);
    }

}

