package dk.cs.au.dwebtek.requests;

import dk.cs.au.dwebtek.Shop;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.Namespace;
import org.jdom2.input.SAXBuilder;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tiami on 08/03/2017.
 */
public class ListShops implements Request {

    private static final Namespace NS = Namespace.getNamespace("http://www.cs.au.dk/dWebTek/2014");

    @Override
    public String getPath() {
        return "/listShops";
    }

    @Override
    public List<Shop> parseResponse(String message) throws JDOMException, IOException {

        SAXBuilder sb = new SAXBuilder();
        Document doc = sb.build(new StringReader(message));
        Element root = doc.getRootElement();

        List<Shop> liste = new ArrayList<>();

        for (Element Shop : root.getChildren()) {
            Shop i = new Shop(0,"","");
            i.setShopID(Integer.parseInt(Shop.getChildText("shopID", NS)));
            i.setShopName(Shop.getChildText("shopName", NS));
            i.setShopURL(Shop.getChildText("shopURL", NS));

            liste.add(i);
        }
        return liste;
    }

}