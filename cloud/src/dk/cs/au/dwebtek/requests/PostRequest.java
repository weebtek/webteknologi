package dk.cs.au.dwebtek.requests;

import dk.cs.au.dwebtek.OperationResult;
import org.jdom2.Document;

/**
 * Created by mortenkrogh-jespersen on 07/02/2017.
 */
public interface PostRequest<E> extends Request<E> {

    OperationResult getPostBody();

}