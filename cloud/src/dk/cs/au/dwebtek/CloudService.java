package dk.cs.au.dwebtek;
import dk.cs.au.dwebtek.requests.*;
import org.jdom2.Document;

import java.util.List;

public class CloudService {

    public OperationResult<Integer> createItem(String itemName) {
        return CloudComm.performRequest(new CreateItem(itemName));
    }

    public OperationResult<List<Item>> listItems(int shopID) {
        return CloudComm.performRequest(new ListItems(shopID));
    }

    public OperationResult<List<Shop>> listShops(){return CloudComm.performRequest(new ListShops());}

    public OperationResult<String> modifyItem(int itemID, String itemName, int itemPrice, String itemURL, String itemDescription) {
        return CloudComm.performRequest(new ModifyItem(itemID, itemName, itemPrice, itemURL, itemDescription));
    }

    public OperationResult<Document> login(String navn, String password){
        return CloudComm.performRequest(new Login(navn, password));
    }

    public OperationResult<Document> sellItems(int itemID, int customerID, int amount) {
        return CloudComm.performRequest(new SellItems(itemID, customerID, amount));
    }

    public OperationResult<Document> createCustomer(String navn, String password){
        return CloudComm.performRequest(new CreateCustomer(navn, password));
    }

    public OperationResult<Void> adjustItemStock(int itemID, int adjustment) {
        return CloudComm.performRequest(new AdjustItemStock(itemID, adjustment));
    }
}