package dk.cs.dwebtek;

import dk.cs.au.dwebtek.CloudService;
import dk.cs.au.dwebtek.Item;
import dk.cs.au.dwebtek.OperationResult;
import dk.cs.au.dwebtek.Shop;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.Namespace;

import org.json.JSONObject;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import java.util.ArrayList;
import java.util.List;

@Path("shop")
@Produces("application/json")
public class ShopService {
    /**
     * Our Servlet session. We will need this for the shopping basket
     */
    private HttpSession session;
    private static final Namespace NS = Namespace.getNamespace("http://www.cs.au.dk/dWebTek/2014");
    private static final int SHOP_ID = 301;
    public ShopService(@Context HttpServletRequest servletRequest) {
        session = servletRequest.getSession();
    }


    @GET
    @Path("items")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Item> getItems() {

        //Getting the items from the cloud server.
        CloudService cs = new CloudService();
        OperationResult<List<Item>> result =  cs.listItems(SHOP_ID);

        if (!result.isSuccess()){
            return new ArrayList<>();
        }
        else {
            return result.getResult();
        }
    }

    @GET
    @Path("shopItems/{shopID}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Item> getShopItems(@PathParam("shopID") String shopID) {

        //Getting the items from the cloud server.
        CloudService cs = new CloudService();
        OperationResult<List<Item>> result =  cs.listItems(Integer.parseInt(shopID));

        if (!result.isSuccess()){
            return new ArrayList<>();
        }
        else {
            return result.getResult();
        }
    }

    @GET
    @Path("shops")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Shop> getShops() {

        //Getting the shops from the cloud server.
        CloudService cs = new CloudService();
        OperationResult<List<Shop>> result =  cs.listShops();

        if (!result.isSuccess()){
            return new ArrayList<>();
        }
        else {
            return result.getResult();
        }
    }

    @POST
    @Path("removeFromBasket/{itemid}")
    @Produces(MediaType.APPLICATION_JSON)
    public String removeFromBasket(@PathParam("itemid") String itemid){
        if(itemid == null){
            return "FEJL: Ingen ItemID";
        }

        //Find the item via itemID

        int id = Integer.parseInt(itemid);
        ArrayList<Item> items = (ArrayList<Item>) session.getAttribute("basket");
        for(Item i:items){
            if(i.getId() == id){

                //if more than one of that item is in the cart
                if(i.getInCart()>1) {
                    i.setInCart(i.getInCart() - 1);
                    return "true";
                }
                else {
                    items.remove(i);
                    return "true";
                }
            }
        }
        return "FEJL: Kunne ikke finde den vare i din kurv.";
    }

    @POST
    @Path("itemsToBasket/{itemid}/{shopid}")
    @Produces(MediaType.APPLICATION_JSON)
    public String inMyBasket(@PathParam("itemid") String itemid,@PathParam("shopid") String shopid){
        if(itemid == null){
            return "FEJL: Ingen ItemID";
        }

        //Check if logged in
        if(!loggedIn()){
            return "Du skal logge ind for at tilføje produkter til din kurv.";
        }

        //Find the item via itemID
        Item item = null;
        int id = Integer.parseInt(itemid);
        List<Item> items = getShopItems(shopid);
        for(Item i:items){
            if(i.getId() == id){
                item = i;
            }
        }

        //doublechecking
        if (item == null){
            return "FEJL: den itemID eksisterer ikke.";
        }

        //if the cart is not empty:
        if(session.getAttribute("basket")!= null){
            ArrayList<Item> basket = (ArrayList<Item>) session.getAttribute("basket");
            for (Item i: basket){

                //check if the item is in the cart already
                if(i.getId()==item.getId()){
                    //if in the cart:

                    //If stock is higher than amount in cart:
                    if(i.getInCart()<i.getStock()) {
                        i.setInCart(i.getInCart() + 1);
                        session.setAttribute("basket", basket);
                        return "true";
                    }

                    //otherwise:
                    else{
                        return "Vi har desværre ikke flere " + i.getName() + " på lager.";
                    }
                }
            }
            //if not in the cart:
           return addToCart(item, basket);
        }

        //if the cart is empty:
        else {
            ArrayList<Item> basket = new ArrayList<>();

        return addToCart(item,basket);
        }
    }

    private String addToCart(Item item, ArrayList<Item> basket){
        //If stock is higher than 0, add to cart.
        if(item.getStock()!= 0) {
            item.setInCart(1);
            basket.add(item);

            session.setAttribute("basket", basket);
            return "true";
        }

        //if stock is 0, return:
        else{
            return "Vi har desværre ingen " + item.getName() + " på lager lige nu.";
        }
    }

    @GET
    @Path("itemsInBasket")
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<Item> basket() {

        if(session.getAttribute("basket")!= null){
            ArrayList<Item> basket = (ArrayList<Item>) session.getAttribute("basket");
            return basket;
        }
        else {
            ArrayList<Item> basket = new ArrayList<>();
            session.setAttribute("basket", basket);
            return basket;
        }
    }


    @GET
    @Path("login/{navn}/{password}")
    public String login(@PathParam("navn") String navn, @PathParam("password") String password) {
       CloudService cs = new CloudService();
       Document loginResponse = (cs.login(navn, password)).getResult();
        String result = checkLogin(loginResponse);
        return result;

    }

    @GET
    @Path("sellItems/{itemID}/{amount}")
    public String sellItems(@PathParam("itemID") String itemID, @PathParam("amount") String amount) {
        CloudService cs = new CloudService();
        Document saleResponse = (cs.sellItems(Integer.parseInt(itemID), customer(),Integer.parseInt(amount))).getResult();
        session.setAttribute("basket",null);
        return saleResponse.toString();

    }

    @GET
    @Path("createCustomer/{navn}/{password}")
    public String createCustomer(@PathParam("navn") String navn, @PathParam("password") String password) {
        CloudService cs = new CloudService();
        Document loginResponse = (cs.createCustomer(navn, password)).getResult();
        return checkLogin(loginResponse);

    }

    private String checkLogin(Document doc){
        if(doc != null)
        {
            JSONObject customer = new JSONObject();
            Element root = doc.getRootElement();
            customer.put("customerID", root.getChildText("customerID",NS));
            customer.put("customerName", root.getChildText("customerName", NS));
            ArrayList<Item> basket = new ArrayList<>();
            session.setAttribute("basket",basket);
            session.setAttribute("customer", customer);
            return "LOGGEDIN";
        }
        else
            return "FEJL";
    }

    @GET
    @Path("loggedIn")
    public boolean loggedIn() {
        if (session.getAttribute("customer") != null) {
            return true;
        }
        else {
            return false;
        }
    }

    private int customer() {
        JSONObject costumer = (JSONObject) session.getAttribute("customer");
        return costumer.getInt("customerID");
    }



}

