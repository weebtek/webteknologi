

function produkter() {
    //shows the product overview in contentDiv.
    document.getElementById("contentDiv").innerHTML='<iframe src="overview.html" />';
}

function showShops() {
    //shows the butik-overview in contentDiv.
    document.getElementById("contentDiv").innerHTML='<iframe src="butikker.html" />';
}

function basket() {

    //check if logged in:
    sendRequest("GET", "rest/shop/loggedIn", null, function (response) {

        //Message if not logged in
        if(response == "false") {
            document.getElementById("contentDiv").innerHTML = '<br/><h1>Du skal logge ind for at købe produkter.</h1><br/>';
        }
        if(response == "true") {

            //shows the basket in contentDiv.
            document.getElementById("contentDiv").innerHTML = '<iframe src="basket.html" />';
        }
    });
}

function showLogin() {
    //shows the login in contentDiv.
    document.getElementById("contentDiv").innerHTML = '<iframe src="login.html" />';

    sendRequest("GET", "rest/shop/loggedIn", null, function (response) {

        //Message if already logged in
        if (response == "true") {
            document.getElementById("contentDiv").innerHTML = '<br/><h3>Du er allerede logget ind. Se din kurv ved at trykke på knappen "Min kurv".</h3>';
        }
    });
}



/////////////////////////////////////////////////////
// Code from slides
/////////////////////////////////////////////////////

/**
 * A function that can add event listeners in any browser
 */
function addEventListener(myNode, eventType, myHandlerFunc) {
    if (myNode.addEventListener)
        myNode.addEventListener(eventType, myHandlerFunc, false);
    else
        myNode.attachEvent("on" + eventType,
            function (event) {
                myHandlerFunc.call(myNode, event);
            });
}

var http;
if (!XMLHttpRequest)
    http = new ActiveXObject("Microsoft.XMLHTTP");
else
    http = new XMLHttpRequest();

function sendRequest(httpMethod, url, body, responseHandler) {
    http.open(httpMethod, url);
    if (httpMethod == "POST") {
        http.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    }
    http.onreadystatechange = function () {
        if (http.readyState == 4 && http.status == 200) {
            responseHandler(http.responseText);
        }
    };
    http.send(body);
}

