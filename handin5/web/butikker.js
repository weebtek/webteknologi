/**
 * Created by tiami on 08/03/2017.
 */

//Run this function when we have loaded the HTML document
window.onload = function () {
    //This code is called when the body element has been loaded and the application starts

    //Request shops from the server. The server expects no request body, so we set it to null
    sendRequest("GET", "rest/shop/shops", null, function (shopsText) {
        //This code is called when the server has sent its data
        var shops = JSON.parse(shopsText);
        addShopsToDropdown(shops);
    });


};

function addShopsToDropdown(shops) {

    //Get the table body we we can add items to it
    var butiklist = document.getElementById("butiklist");

    //Remove all contents of the table body (if any exist)
    butiklist.innerHTML = "";

    //Loop through the shops from the server
    for (var i = 0; i < shops.length; i++) {
        var shop = shops[i];

        //Create a new line for this shop
        var shoprow = document.createElement("tr");

        //Create a new link for this shop
        var shopcol = document.createElement("td");
        var link = document.createElement("BUTTON");
        link.setAttribute("class","linkButton");
        var shopNameText = document.createTextNode(shop.shopName);
        link.appendChild(shopNameText);
        shopcol.appendChild(link);
        shoprow.appendChild(shopcol);

        //Create a hidden shopID cell
        var idrow = document.createElement("td");
        idrow.setAttribute("class", "shopTD");
        var idtext = document.createTextNode(shop.shopID);
        idrow.appendChild(idtext);
        shoprow.appendChild(idrow);

        butiklist.appendChild(shoprow);

        // Eventlistener for shop link
        var shopAttributes = shoprow.childNodes;
        var thisID = shopAttributes[1].firstChild.nodeValue;
        link.setAttribute("onClick", 'shopProdukt(' + thisID + ')');
    }
}

function shopProdukt(thisID){
    //Fetching items from the chosen shop
    sendRequest("GET", "rest/shop/shopItems/" + thisID, null, function (resText) {
        //This code is called when the server has sent its data
        var shops = JSON.parse(resText);
        addItemsToShopTable(shops, thisID);
    });
}

 function addItemsToShopTable(items, idShop) {

    //Get the table body we we can add items to it
    var tableBody = document.getElementById("shopTable");

    //Remove all contents of the table body (if any exist)
    tableBody.innerHTML = "";

    //Loop through the items from the server
    for (var i = 0; i < items.length; i++) {
        var item = items[i];

        //Create a new line for this item
        var tr = document.createElement("tr");

        var nameCell = document.createElement("td");
        nameCell.textContent = item.name;
        tr.appendChild(nameCell);

        var idCell = document.createElement("td");
        idCell.textContent = item.id;
        tr.appendChild(idCell);

        var priceCell = document.createElement("td");
        priceCell.textContent = item.price + " kr";
        tr.appendChild(priceCell);

        var imgCell = document.createElement("td");
        imgCell.innerHTML = '<img src=' + item.url + ' height="100" width="100">';
        tr.appendChild(imgCell);

        var desCell = document.createElement("td");
        desCell.setAttribute("claass","dropShop")
        desCell.innerHTML = item.itemDescription;
        tr.appendChild(desCell);

        var stockCell = document.createElement("td");
        stockCell.textContent = item.stock;
        tr.appendChild(stockCell);

        var cartCell = document.createElement("td");
        var btn = document.createElement("BUTTON");
        var text = document.createTextNode("Læg i kurv");
        btn.setAttribute("class", "shopbutt");
        addEventListener(btn, "click", function () {

            //Add item to cart via JAX serveren.
            var attributes = (this.parentNode).parentNode.childNodes;
            var thisID = parseInt(attributes[1].textContent);
            console.log("før-request tjek: " + thisID);

            sendRequest("POST", "rest/shop/itemsToBasket/" + thisID + "/" + idShop, null, function (result) {

                //This code is called when the server has sent its data
                if (result == "true") {
                    window.alert(attributes[0].textContent + " er tilføjet til din kurv.")
                }
                else {
                    window.alert(result);
                }
            });
        });
        btn.appendChild(text);
        cartCell.appendChild(btn);
        tr.appendChild(cartCell);

        tableBody.appendChild(tr);
    }
}