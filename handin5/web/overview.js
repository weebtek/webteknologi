/**
 * Created by tiami on 07/03/2017.
 */

//Run this function when we have loaded the HTML document
window.onload = function () {
    //This code is called when the body element has been loaded and the application starts

    //Request items from the server. The server expects no request body, so we set it to null
    sendRequest("GET", "rest/shop/items", null, function (itemsText) {
        //This code is called when the server has sent its data
        var items = JSON.parse(itemsText);
        addItemsToTable(items);
    });

};


function addItemsToTable(items) {

    //Get the table body we we can add items to it
    var tableBody = document.getElementById("itemtablebody");

    //Remove all contents of the table body (if any exist)
    tableBody.innerHTML = "";

    //Loop through the items from the server
    for (var i = 0; i < items.length; i++) {
        var item = items[i];

        //Create a new line for this item
        var tr = document.createElement("tr");

        var nameCell = document.createElement("td");
        nameCell.textContent = item.name;
        tr.appendChild(nameCell);

        var idCell = document.createElement("td");
        idCell.textContent = item.id;
        tr.appendChild(idCell);

        var priceCell = document.createElement("td");
        priceCell.textContent = item.price + " kr";
        tr.appendChild(priceCell);

        var imgCell = document.createElement("td");
        imgCell.innerHTML = '<img src=' + item.url + ' height="100" width="100">';
        tr.appendChild(imgCell);

        var desCell = document.createElement("td");
        desCell.innerHTML = item.itemDescription;
        tr.appendChild(desCell);

        var stockCell = document.createElement("td");
        stockCell.textContent = item.stock;
        tr.appendChild(stockCell);

        var cartCell = document.createElement("td");
        var btn = document.createElement("BUTTON");
        var text = document.createTextNode("Læg i kurv");
        btn.setAttribute("class", "buttbutt");
        addEventListener(btn, "click", function () {

            //Add item to cart via JAX serveren.
            var attributes = (this.parentNode).parentNode.childNodes;
            var thisID = parseInt(attributes[1].textContent);
            console.log("før-request tjek: " + thisID);

            sendRequest("POST", "rest/shop/itemsToBasket/" + thisID + "/301", null, function (result) {

                //This code is called when the server has sent its data
                if (result=="true") {
                    window.alert(attributes[0].textContent + " er tilføjet til din kurv.")
                }
                else {
                    window.alert(result);
                }
            });
        });
        btn.appendChild(text);
        cartCell.appendChild(btn);
        tr.appendChild(cartCell);

        tableBody.appendChild(tr);
    }

}
