/**
 * Created by tiami on 09/03/2017.
 */
//Run this function when we have loaded the HTML document
window.onload = function () {
    //This code is called when the body element has been loaded and the application starts

    // Eventlistener for login
    var loginButton = document.getElementById("loginButton");
    addEventListener(loginButton, "click", function () {
        var navn = document.getElementById("navn");
        var password = document.getElementById("password");

        sendRequest("GET", "rest/shop/login/" + navn.value + "/" + password.value, null, function (response) {
            if (response == "LOGGEDIN") {
                window.alert("Du er logget ind");

                //Message when logged in
                document.getElementById("loginWrapper").innerHTML =
                    '<h3>'+ navn.value + ', du er logget ind. Se din kurv ved at trykke på knappen "Min kurv"!</h3>';
            }

            //Message when login failed
            if (response == "FEJL") {
                window.alert("Der er sket en fejl. Forkert brugernavn eller kode.");
            }
        });
    });

    //Eventlistener for create customer.
    // You automatically log in when you create a new customer.
    var createCustomerButton = document.getElementById("createCustomerButton");
    addEventListener(createCustomerButton, "click", function () {
        var navn = document.getElementById("navn");
        var password = document.getElementById("password");

        sendRequest("GET", "rest/shop/createCustomer/" + navn.value + "/" + password.value, null, function (response) {

            if (response == "LOGGEDIN") {
                window.alert("Du er logget ind");

                //Message when logged in
                document.getElementById("loginWrapper").innerHTML =
                    '<h3>'+ navn.value + ', du er logget ind. Se din kurv ved at trykke på knappen "Min kurv"!</h3>';
            }
            if (response == "FEJL") {
                window.alert("Kunne ikke oprette bruger.");
            }
        });
    });


};