/**
 * Created by tiami on 07/03/2017.
 */
window.onload = function() {

    //Request basket from JAX RS.
    sendRequest("GET", "rest/shop/itemsInBasket", null, function (basketText) {

        //This code is called when the server has sent its data
        var basketArray = JSON.parse(basketText);
        addItemsToBasket(basketArray);

        // Eventlistener for buybutton
        var buyButton = document.getElementById("buyButton");
        addEventListener(buyButton, "click", function () {

            if(basketArray.length < 1){
                window.alert("Ingen produkter til at købe")
            }

            else {
                //for every item in the cart
                for (var i = 0; i < basketArray.length; i++) {
                    var item = basketArray[i];

                    //sell items
                    sendRequest("GET", "/rest/shop/sellItems/" + item.id + "/" + item.inCart, null, function (response) {
                        if (response != null) {

                            //cart is emptied and a message shown.
                            document.getElementById("baskettablebody").innerHTML = "";
                            var trow = document.createElement("tr");
                            var tcol = document.createElement("td");
                            tcol.setAttribute("colspan", 3);
                            var txt = document.createTextNode("Tillykke med købet.");
                            trow.appendChild(tcol);
                            tcol.appendChild(txt);
                            document.getElementById("baskettablebody").appendChild(trow);

                            //price set to 0
                            document.getElementById("pris").innerHTML = 0;

                            //sets the local variable with cart-items to an empty array.
                            basketArray = [];

                            window.alert("Du har nu købt produkterne.");
                        }
                    });
                }

            }
        });
    });

};

function addItemsToBasket(basketArray) {

    //Get the table body we we can add items to it
    var tableBody = document.getElementById("baskettablebody");

    //Remove all contents of the table body (if any exist)
    tableBody.innerHTML = "";

    var price = 0;

    //Show message if cart is empty
    if(basketArray.length == 0){
        var trow = document.createElement("tr");
        var tcol = document.createElement("td");
        tcol.setAttribute("colspan",3);
        var txt = document.createTextNode("Du har ikke noget i din kurv.");
        trow.appendChild(tcol);
        tcol.appendChild(txt);
        tableBody.appendChild(trow);
    }

    //Loop through the items from the server
    for (var i = 0; i < basketArray.length; i++) {
        var item = basketArray[i];
        //Create a new line for this item
        var tr = document.createElement("tr");

        var nameCell = document.createElement("td");
        nameCell.textContent = item.name;
        tr.appendChild(nameCell);

        var idCell = document.createElement("td");
        idCell.textContent = item.id;
        tr.appendChild(idCell);

        var priceCell = document.createElement("td");
        priceCell.textContent = item.price + " kr";
        tr.appendChild(priceCell);

        var imgCell = document.createElement("td");
        imgCell.innerHTML = '<img src=' + item.url + ' height="50" width="50">';
        tr.appendChild(imgCell);

        var stockCell = document.createElement("td");
        stockCell.textContent = item.stock;
        tr.appendChild(stockCell);

        var inCartCell = document.createElement("td");
        inCartCell.textContent = item.inCart;
        tr.appendChild(inCartCell);

        var cartCell = document.createElement("td");
        var btn = document.createElement("BUTTON");
        var text = document.createTextNode("Fjern fra kurv");
        addEventListener(btn, "click", function () {

            //Remove item from cart via JAX serveren.
            var attributes = (this.parentNode).parentNode.childNodes;
            var thisID = parseInt(attributes[1].textContent);
            console.log("remove-request tjek: " + thisID);

            sendRequest("POST", "rest/shop/removeFromBasket/" + thisID, null, function (result) {

                //This code is called when the server has sent its data
                if (result=="true") {
                    console.log("1 styk " + attributes[0].textContent + " er fjernet fra din kurv.");

                    //price set to 0
                    document.getElementById("pris").innerHTML = 0;

                    //shows the basket in contentDiv.
                    sendRequest("GET", "rest/shop/itemsInBasket", null, function (basketText) {

                        //This code is called when the server has sent its data
                        var basketArray = JSON.parse(basketText);
                        addItemsToBasket(basketArray);

                    });
                }
                else {
                    window.alert(result);
                }
            });
        });
        btn.appendChild(text);
        cartCell.appendChild(btn);
        tr.appendChild(cartCell);

        tableBody.appendChild(tr);

        //We update the price
        price = price + item.price*item.inCart;
        document.getElementById("pris").innerHTML = price;
    }

}
