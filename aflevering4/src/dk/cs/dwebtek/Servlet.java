package dk.cs.dwebtek;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Anne on 25/02/2017.
 */
public class Servlet extends HttpServlet {

    private String code, username;
    private final static String SERVER_URL = "http://webtek.cs.au.dk/projects/au558645/dev";

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        code = request.getParameter("code");
        username = request.getParameter("username");
        System.out.println("code: " + code + ", username: " + username);

        URL url = new URL("https://services.brics.dk/java/dovs-auth/token");

        if (!code.isEmpty() && !username.isEmpty()) {
            String postData = "grant_type=authorization_code"
                                + "&code=" + code
                                + "&client_id=webtek"
                                + "&client_secret=webtek"
                                + "&redirect_uri=http://webtek.cs.au.dk/projects/au558645/dev/oauth-callback";
            // Setup the connection
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setDoInput(true);
            // 15 second timeout
            httpURLConnection.setReadTimeout(15*1000);
            httpURLConnection.connect();
            // Send data
            DataOutput dataOutput = new DataOutputStream(httpURLConnection.getOutputStream());
            dataOutput.write(postData.getBytes());
            // Read response
            if (httpURLConnection.getResponseCode() == 200) {
                System.out.println("Response code: " + httpURLConnection.getResponseCode());
                HttpSession session = request.getSession(true);
                session.setAttribute("username", username);
                session.setAttribute("code", code);
                response.sendRedirect(SERVER_URL + "/overview.xhtml");
            } else {
                System.out.println("Response code: " + httpURLConnection.getResponseCode());
                response.sendRedirect(SERVER_URL + "/log-in.xhtml");
            }

        }
    }
}
