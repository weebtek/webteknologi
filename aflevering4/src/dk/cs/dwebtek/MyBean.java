package dk.cs.dwebtek;

import dk.cs.au.dwebtek.CloudComm;
import dk.cs.au.dwebtek.CloudService;
import dk.cs.au.dwebtek.Item;
import dk.cs.au.dwebtek.OperationResult;
import javax.faces.bean.SessionScoped;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.servlet.http.HttpSession;

import org.jdom2.Document;
import org.jdom2.JDOMException;
import org.jdom2.Namespace;
import org.jdom2.input.SAXBuilder;
import org.jdom2.input.sax.XMLReaderJDOMFactory;
import org.jdom2.input.sax.XMLReaderXSDFactory;
import org.jdom2.output.XMLOutputter;

import java.io.IOException;
import java.io.StringReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by tiami on 21/02/2017.
 */
@ManagedBean
@SessionScoped
public class MyBean {
    private static final String SHOP_KEY = "00FF0887059C03DCC03AB517";
    private static final Namespace NS = Namespace.getNamespace("http://www.cs.au.dk/dWebTek/2014");
    private String itemName;
    private List<Item> items;
    private int id;
    private String name;
    private int price;
    private String url;
    private String itemDescription;
    private int stock;

    public MyBean(){
        refreshList();
    }

    public boolean validateHTML() {
        SAXBuilder sb = new SAXBuilder();
        Document doc = null;
        try {
            doc = sb.build(new StringReader(itemDescription));
        } catch (JDOMException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        OperationResult val = validate(doc);

        if(val.isSuccess()) return true;
        else return false;
    }

    public String modify() {

        // if(!validaring) - throw some exception eller errormessage. If success, gå videre i koden:

        OperationResult res = new CloudService().modifyItem(id, name, price, url, itemDescription);
        if (res.isSuccess()) {
            System.out.println("Successfully modified item");
        } else {
            System.out.println(res.getMessage());
        }

        refreshList();
        return "BACK";
    }

    public String stock(){

        //Her skal vi kommunikere med Cloud, hente vores Stock, og opdatere det til at være lig med lokalvariablet "stock".
        OperationResult res = new CloudService().adjustItemStock(id, stock);
        if (res.isSuccess()) {
            System.out.println("Successfully updated stock");
            System.out.println("itemID: " + items.get(0).getId() + ", stock: " + items.get(0).getStock());
        } else {
            System.out.println(res.getMessage());
        }
        refreshList();
        return "BACK";
    }

    public String createItem(){

        OperationResult<Integer> result = new CloudService().createItem(itemName);
        if (result.isSuccess()) {
            System.out.printf("Result: " + result.getResult() + "\n");
        } else {
            System.out.println(result.getMessage());
        }

        return "EDIT";
    }

    public void refreshList(){
        OperationResult<List<Item>> result =  new CloudService().listItems();
        if (result.isSuccess()){ items = result.getResult();}
        else {
            items = new ArrayList<>();
            System.out.println("Failed refreshing the list");
        }
    }
    public String edit(int i) {

        id = i;

        return "EDIT";
    }
    public String create(){
        return "CREATE";
    }

    public String back(){
        refreshList();
        return "BACK";
    }

    private OperationResult<Object> validate(Document doc) {

        URL url = getClass().getClassLoader().getResource("cloud.xsd");
        XMLReaderJDOMFactory factory;

        try {
            factory = new XMLReaderXSDFactory(url);
        } catch (JDOMException e) {
            return OperationResult.Fail("Could not find schema");
        }

        String xml = new XMLOutputter().outputString(doc);
        SAXBuilder builder = new SAXBuilder(factory);
        try {
            builder.build(new StringReader(xml));
        } catch (JDOMException e) {
            return OperationResult.Fail("Xml is not valid: " + e.getMessage());
        } catch (IOException e) {
            return OperationResult.Fail("YIKES: " + e.getMessage());
        }
        return OperationResult.Success(true);
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String name){
        itemName = name;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String description) {
        this.itemDescription = description;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }
}