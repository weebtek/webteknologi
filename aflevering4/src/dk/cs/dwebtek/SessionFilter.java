package dk.cs.dwebtek;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Anne on 25/02/2017.
 */
public class SessionFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        String uri = ((HttpServletRequest) servletRequest).getRequestURI();
        // Alle der er logget ind er admins :)
        if (uri.endsWith("/overview.xhtml") && ((HttpServletRequest) servletRequest).getSession().getAttribute("username") == null) {
            ((HttpServletResponse) servletResponse).sendRedirect("http://webtek.cs.au.dk/projects/au558645/dev/log-in.xhtml");
        }
        if (uri.endsWith("/log-in.xhtml") && ((HttpServletRequest) servletRequest).getSession().getAttribute("username") != null) {
            ((HttpServletResponse) servletResponse).sendRedirect("http://webtek.cs.au.dk/projects/au558645/dev/overview.xhtml");
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {

    }
}
