import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by tiami on 21/02/2017.
 */
@ManagedBean
@SessionScoped
public class BeerSongSession {

    private int verses;
    private List<Integer> song;

    public String makeSong() {
        if(getVerses()>0){

            int end = getVerses() + 1;
            song = IntStream.range(1, end).map(i -> end - i).boxed().collect(Collectors.toList());

        } else {

            song = new ArrayList<>();

        }
        return "SHOW_SONG";
    }

    public int getVerses() {
        return verses;
    }

    public void setVerses(int verses) {
        this.verses = verses;
    }

    public List<Integer> getSong() {
        return song;
    }

    public void setSong(List<Integer> song) {
        this.song = song;
    }
}
